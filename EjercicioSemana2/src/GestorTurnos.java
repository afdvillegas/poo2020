import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class GestorTurnos {

	private ArrayList<Paciente> pacientes = new ArrayList<>();

	public GestorTurnos() {
		super();
		this.pacientes = new ArrayList<Paciente>();
	}

	public ArrayList<Paciente> getPacientes() {
		return pacientes;
	}

	public void setPacientes(ArrayList<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

//Agrego al paciente haciendo de la altura y peso sean aleatorias
	public void agregarPaciente() {

		Scanner s = new Scanner(System.in);
		String nombre;
		String apellido;
		Character sexo;
		Integer edad;
		Double peso;
		Double altura;

		System.out.println("Ingrese nombre paciente: ");
		nombre = s.nextLine();

		System.out.println("Ingrese apellido paciente: ");
		apellido = s.nextLine();

		System.out.println("Ingrese sexo: M(Masculino) - F(Femenino)");

		sexo = s.next().charAt(0);

		System.out.println("Ingrese edad: ");
		edad = s.nextInt();

		peso = (Math.random() * 80) + 20;
		System.out.println("Peso: " + peso);

		altura = (Math.random() * 1.5) + 1;
		System.out.println("Altura: " + altura);

		// numero = (int) (Math.random() * n) + 1;
		// Donde n es hasta el n�mero que quieres que llegue, + 1 para que nunca tengas
		// un 0 y el cast a int porque Math#random arroja un valor de tipo double.

		Paciente p = new Paciente(nombre, apellido, sexo, edad, peso, altura);
		pacientes.add(p);
		System.out.println("Se a�ade un paciente nuevo");

	}

	// Numero y muestro a todos los pacientes
	public ArrayList<Paciente> listarPacientes() {
		int i = 1;
		for (Paciente paciente : pacientes) {
			System.out.println("Paciente numero " + i);
			System.out.println(paciente.getNombre());
			System.out.println(paciente.getApellido());
			i++;
		}
		return pacientes;
	}

//Una vez agregado los pacientes, les asigno un turno por cada media hora del dia de hoy
	// Y los voy guardando en un txt de la carpeta assets
	public void asignarTurnosArchivo() {
		String texto = "";
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);

		try (Formatter output = new Formatter("assets/pacientes.txt")) {

			for (Paciente paciente : pacientes) {
				texto = "El paciente " + paciente.getNombre() + " " + paciente.getApellido()
						+ " tiene turno a la fecha y hora: " + c.getTime() + "\n";
				c.add(Calendar.MINUTE, 30);

				try {
					output.format("%s", texto);

				} catch (NoSuchElementException e) {
					System.out.println("El valor ingresado no es valido");
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	
	//Calcula las alturas promedios seg�n el sexo y las muestra
	public void listaAlturaPromedioSexo() {
		Double alturaPromM = 0.0;
		Double alturaPromF = 0.0;
		Integer i = 0;
		Integer j = 0;
		for (Paciente paciente : pacientes) {
			if (paciente.getSexo() == 'M') {
				i = i + 1;
				alturaPromM = alturaPromM + paciente.getAltura();
			} else {
				j = j + 1;
				alturaPromF = alturaPromF + paciente.getAltura();
			}
		}
		alturaPromM = alturaPromM / i;
		alturaPromF = alturaPromF / j;
		System.out.println("La altura promedio de los pacientes de sexo Masculino es de: ");
		System.out.println(alturaPromM);
		System.out.println("La altura promedio de los pacientes de sexo Femenino es de: ");
		System.out.println(alturaPromF);
	}

	//Pide ingresar una edad y luego calcula el peso promedio de esa edad para mostrarlo
	public void listaPesoPromedioEdad() {
		Scanner s = new Scanner(System.in);
		Integer edad;
		Double pesoProm = 0.0;
		Integer i = 0;

		System.out.println("Ingrese edad: ");
		edad = s.nextInt();

		for (Paciente paciente : pacientes) {
			if (edad == paciente.getEdad()) {
				pesoProm = pesoProm + paciente.getPeso();
				i++;
			}
		}
		pesoProm = pesoProm / i;
		System.out.println("El peso promedio de los pacientes con " + edad + " a�os es de " + pesoProm);
	}

}
