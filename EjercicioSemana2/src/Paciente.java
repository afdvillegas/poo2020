import java.util.Random;

public class Paciente {

	private String nombre;
	private String apellido;
	private Character sexo;
	private Integer edad;
	private Double peso;
	private Double altura;

	public Paciente(String nombre, String apellido, Character sexo, Integer edad, Double peso, Double altura) {
		super();
		this.setNombre(nombre);

		this.setApellido(apellido);

		this.setSexo(sexo);

		this.setEdad(edad);

		this.setPeso(peso);

		this.setAltura(altura);

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Character getSexo() {
		return sexo;
	}

	public void setSexo(Character sexo) {
		this.sexo = sexo;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {

		this.peso = peso;
	}

	public Double getAltura() {
		return altura;
	}

	public void setAltura(Double altura) {

		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Paciente [nombre=" + nombre + ", apellido=" + apellido + ", sexo=" + sexo + ", edad=" + edad + ", peso="
				+ peso + ", altura=" + altura + "]";
	}

}
