
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Formatter;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Main {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		GestorTurnos g = new GestorTurnos();
		Boolean rta = true;

		// Bucle que pide agregar pacientes hasta que se ingrese 'N'

		while (rta == true) {
			g.agregarPaciente();
			System.out.println("�Desea agregar otro paciente? Pulse N para terminar");
			Character letra;
			letra = s.nextLine().charAt(0);

			if (letra == 'N') {
				rta = false;
			}

		}

//Selector Joption

		Object seleccion = null;

		seleccion = JOptionPane.showInputDialog(null, "Seleccione opcion", "Menu", JOptionPane.QUESTION_MESSAGE, null,
				new Object[] { "1: Listar pacientes", "2: Listar alturas promedio segun el sexo",
						"3: Listar peso promedio seg�n la edad", "4: Asignar turnos de pacientes a un archivo" },
				"5: Cancelar");

		if (seleccion == null) {
			System.out.println("Cancelado");
		} else {
			char nro = ((String) seleccion).charAt(0);
			System.out.println(nro);

			System.out.println("El usuario ha elegido " + seleccion);
			switch (nro) {
			case '1':
				g.listarPacientes();
				break;
			case '2':
				g.listaAlturaPromedioSexo();
				break;
			case '3':
				g.listaPesoPromedioEdad();

				break;
			case '4':
				g.asignarTurnosArchivo();
				break;

			default:
				System.out.println("No ingres� ningun valor valido");
				break;
			}

		}

	}
}
