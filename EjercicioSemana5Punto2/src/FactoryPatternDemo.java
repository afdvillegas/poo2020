
public class FactoryPatternDemo {

	public static void main(String[] args) {
		ShapeFactory factory = new ShapeFactory();
		Shape forma1 = factory.getShape("circle");
		Shape forma2 = factory.getShape("square");
		Shape forma3 = factory.getShape("rectangule");
		forma1.Draw();
		forma2.Draw();
		forma3.Draw();
	}

}
