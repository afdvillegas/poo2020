package controlador;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import vista.vistaRompecabezas;

public class ControladorRompecabezas implements MouseMotionListener, MouseListener{
	private vistaRompecabezas vista;

	public ControladorRompecabezas() {
		super();
		this.vista = new vistaRompecabezas(this);
		this.vista.setVisible(true);
	}

	public vistaRompecabezas getVista() {
		return vista;
	}

	public void setVista(vistaRompecabezas vista) {
		this.vista = vista;
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
