package controlador;

import java.awt.EventQueue;

import vista.vistaRompecabezas;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorRompecabezas();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
