package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorRompecabezas;

public class vistaRompecabezas extends JFrame {

	private JPanel contentPane;
	private ControladorRompecabezas c;

	private JLabel containerImg;

	public vistaRompecabezas(ControladorRompecabezas c) {
		this.setC(c);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 544, 418);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		try {
			BufferedImage img = ImageIO.read(new File("img\\tom.jpg"));

			containerImg = new JLabel(new ImageIcon(img));
			containerImg.setBounds(0, 0, 544, 418);
			// contentPane.add(containerImg);

			BufferedImage cortada = cortar(img, 0, 0, 273, 417);
			JLabel imagenCortada = new JLabel(new ImageIcon(cortada));
			imagenCortada.setBounds(273, 0, 273, 417);
			// imagenCortada.setBounds(40, 10, 400, 286);
			contentPane.add(imagenCortada);

			BufferedImage cortada2 = cortar(img, 270, 0, 273, 417);
			JLabel imagenCortada2 = new JLabel(new ImageIcon(cortada2));

			imagenCortada2.setBounds(0, 0, 273, 417);
			contentPane.add(imagenCortada2);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		repaint();
	}

	public ControladorRompecabezas getC() {
		return c;
	}

	public void setC(ControladorRompecabezas c) {
		this.c = c;
	}

	public BufferedImage cortar(BufferedImage image, int startX, int startY, int endX, int endY) {
		BufferedImage img = image.getSubimage(startX, startY, endX, endY);
		BufferedImage copyOfImage = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
		Graphics g = copyOfImage.createGraphics();
		g.drawImage(img, 0, 0, null);
		return copyOfImage;
	}

	public JLabel getContainerImg() {
		return containerImg;
	}

	public void setContainerImg(JLabel containerImg) {
		this.containerImg = containerImg;
	}

}
