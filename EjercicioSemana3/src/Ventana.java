import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

public class Ventana extends JFrame {

	private JPanel contentPane;
	private JTextField textURL;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(128, 128, 128));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblUrl = new JLabel("URL");
		lblUrl.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUrl.setBounds(94, 58, 62, 14);
		contentPane.add(lblUrl);
		
		textURL = new JTextField();
		textURL.setBackground(new Color(255, 255, 255));
		textURL.setBounds(195, 55, 148, 20);
		contentPane.add(textURL);
		textURL.setColumns(10);
		
		JButton btnBoton = new JButton("Boton");
		btnBoton.setBackground(Color.WHITE);
		btnBoton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
	
				
				obtenerImagen();
			}
		});
		btnBoton.setBounds(167, 228, 89, 23);
		contentPane.add(btnBoton);
		
		JLabel lblTitulo = new JLabel("Mostrar imagen");
		lblTitulo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTitulo.setBounds(167, 11, 130, 33);
		contentPane.add(lblTitulo);

		

		setLocationRelativeTo(null);
	}

	protected void obtenerImagen() {
		ImageIcon imageIcon = new ImageIcon(new ImageIcon(textURL.getText()).getImage().getScaledInstance(117, 79, Image.SCALE_DEFAULT));

		//ImageIcon imageIcon = new ImageIcon(new ImageIcon("img\\gordon.jpg").getImage().getScaledInstance(117, 79, Image.SCALE_DEFAULT));
		JLabel lblImagen = new JLabel("");
		lblImagen.setBounds(149, 116, 117, 79);
		lblImagen.setIcon(imageIcon);
		contentPane.add(lblImagen);
		//Actualiza el icono
		repaint();
		
		
	}

	
}
/*
se podria controlar la url chequeando si es un directorio y si el archivo existe previamente asi: 
 File file = new File(txtDireccion.getText());
if (!file.isDirectory())
  file = file.getParentFile();
if (file.exists()){
   ...
}*/
