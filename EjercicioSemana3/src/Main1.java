import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main1 {

	public static void main(String[] args) {
		URL url;

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
		gsonBuilder.registerTypeAdapter(LocalDate.class, new LocalDateDeserializer());
		Gson gson = gsonBuilder.setPrettyPrinting().create();

		try {

			url = new URL("https://raw.githubusercontent.com/alevega/datosCambio/master/datos.json");

			URLConnection conexion = url.openConnection();

			BufferedReader lectura = new BufferedReader(new InputStreamReader(conexion.getInputStream()));

			String linea;
			String datos = "";

			while ((linea = lectura.readLine()) != null) {

				datos = datos + linea;

			}

			Map jsonOb = (Map) gson.fromJson(datos, Object.class);
			List map = (List) jsonOb.get("cambio");

			ArrayList<Datos> da = new ArrayList<>();

			for (int i = 0; i < map.size(); i++) {
				da.add(gson.fromJson(map.get(i).toString(), Datos.class));
			}
			Scanner s = new Scanner(System.in);
			Boolean ciclo = true;
			int x = 0;
			//Uso de excepciones
			do {
				try {
					System.out.println("Ingrese cuantas compras va a realizar: ");
					x = s.nextInt();
					ciclo = false;
				} catch (InputMismatchException e) {
					System.out.println("Excepcion: " + e);
					s.nextLine();
					System.out.println("Ingrese numero entero");

				}

			} while (ciclo);

			// Paso a hashmap
			Cambio c = new Cambio(da);

			CuentaBancaria cb = new CuentaBancaria(100.00);
			System.out.println("Pesos antes de la compra: ");
			System.out.println(cb.getPesos());
			System.out.println("Dolares antes de la compra: ");
			System.out.println(cb.getDolares());
			System.out.println("///////////////////////////////");

			cb.compraDolares(x, c);
			System.out.println("Pesos actuales: ");
			System.out.println(cb.getPesos());
			System.out.println("Dolares actuales: ");
			System.out.println(cb.getDolares());
			System.out.println("///////////////////////////////");

			cb.ventaDolares(x, c);
			System.out.println("Pesos actuales: ");
			System.out.println(cb.getPesos());
			System.out.println("Dolares actuales: ");
			System.out.println(cb.getDolares());

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
