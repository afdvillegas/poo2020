import java.time.LocalDate;

public class Datos {

	private LocalDate fecha;
	private Double cambio;

	public Datos(LocalDate fecha, Double cambio) {
		super();
		this.setFecha(fecha);
		this.setCambio(cambio);
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public Double getCambio() {
		return cambio;
	}

	public void setCambio(Double cambio) {
		this.cambio = cambio;
	}
}
