import java.security.SecureRandom;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class CuentaBancaria {

	private Double dolares;
	private Double pesos;

	public CuentaBancaria(Double dolares) {
		super();
		this.setDolares(dolares);
		double valor = (Math.random() * 3000) + 1000;
		this.setPesos(valor);
	}

	public Double getDolares() {
		return dolares;
	}

	public void setDolares(Double dolares) {
		this.dolares = dolares;
	}

	public Double getPesos() {
		return pesos;
	}

	public void setPesos(Double pesos) {
		this.pesos = pesos;
	}

	public void compraDolares(Integer cantidad, Cambio c) {

		// Inicializa el primer dia
		LocalDate fecha = LocalDate.of(2018, 07, 01);

		SecureRandom r = new SecureRandom();

		while (cantidad > 0) {

			fecha = fecha.plusDays(r.nextInt(41) + 1);
			Integer cantidadDolares = r.nextInt(10);
			Double valorDolar = c.getCambio().get(fecha);
			System.out.println("Compra nro: " + cantidad);

			cantidad = cantidad - 1;
			System.out.println("El dolar vale: " + valorDolar + " pesos");
			this.setPesos(this.getPesos() - (valorDolar));
			System.out.println("Compro " + cantidadDolares + " dolares");
			this.setDolares(this.getDolares() + cantidadDolares);
		}

	}

	public void ventaDolares(Integer cantidad, Cambio c) {
		SecureRandom r = new SecureRandom();
		LocalDate fecha = LocalDate.of(2018, 07, 01);

		while (cantidad > 0) {
			fecha = fecha.plusDays(r.nextInt(41) + 1);
			Integer cantidadDolares = r.nextInt(10);
			Double valorDolar = c.getCambio().get(fecha);
			System.out.println("Venta nro: " + cantidad);

			cantidad = cantidad - 1;

			System.out.println("Vendio " + cantidadDolares + " dolares");
			this.setDolares(this.getDolares() - cantidadDolares);

			System.out.println("El dolar vale: " + valorDolar + " pesos");
			this.setPesos(this.getPesos() + valorDolar);

		}

	}
}
