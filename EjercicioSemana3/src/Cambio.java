import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class Cambio {
	HashMap<LocalDate, Double> cambio = new HashMap();

	public Cambio(ArrayList<Datos> datos) {
		super();
		for (Datos dato : datos) {
			this.getCambio().put(dato.getFecha(), dato.getCambio());
		}

	}

	public HashMap<LocalDate, Double> getCambio() {
		return cambio;
	}

	public void setCambio(HashMap<LocalDate, Double> cambio) {
		this.cambio = cambio;
	}

}
