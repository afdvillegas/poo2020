package controlador;

import java.awt.EventQueue;

import vista.vistaCalculadora;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new ControladorCalculadora();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
