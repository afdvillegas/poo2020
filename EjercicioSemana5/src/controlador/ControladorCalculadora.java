package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.util.ArrayList;

import modelo.Calculadora;
import vista.vistaCalculadora;

public class ControladorCalculadora implements ActionListener {
	private vistaCalculadora vista;
	private Calculadora calculadora;

	public ControladorCalculadora() {
		super();
		this.vista = new vistaCalculadora(this);
		this.calculadora = new Calculadora();

		this.vista.setVisible(true);
	}

	public vistaCalculadora getVista() {
		return vista;
	}

	public void setVista(vistaCalculadora vista) {
		this.vista = vista;
	}

	public Calculadora getCalculadora() {
		return calculadora;
	}

	public void setCalculadora(Calculadora calculadora) {
		this.calculadora = calculadora;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		String numerosText = "";

		if (e.getSource().equals(vista.getBtn0())) {
			numerosText = numerosText + "0";
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "0");
		}
		if (e.getSource().equals(vista.getBtn1())) {
			numerosText = numerosText + "1";
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "1");
		}
		if (e.getSource().equals(vista.getBtn2())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "2");
		}
		if (e.getSource().equals(vista.getBtn3())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "3");
		}
		if (e.getSource().equals(vista.getBtn4())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "4");
		}
		if (e.getSource().equals(vista.getBtn5())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "5");
		}
		if (e.getSource().equals(vista.getBtn6())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "6");
		}
		if (e.getSource().equals(vista.getBtn7())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "7");
		}
		if (e.getSource().equals(vista.getBtn8())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "8");
		}
		if (e.getSource().equals(vista.getBtn9())) {
			vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + "9");
		}

		if (e.getSource().equals(vista.getBtnPunto())) {
			String cadena = vista.getTextFieldIngreso().getText();
			if (cadena.length() <= 0) {
				vista.getTextFieldIngreso().setText("0.0");
			} else {
				if (!existePunto(vista.getTextFieldIngreso().getText())) {
					vista.getTextFieldIngreso().setText(vista.getTextFieldIngreso().getText() + ".");
				}
			}
		}
		if (e.getSource().equals(vista.getBtnBack())) {
			String cadena = vista.getTextFieldIngreso().getText();

			if (cadena.length() > 0) {
				cadena = cadena.substring(0, cadena.length() - 1);
				vista.getTextFieldIngreso().setText(cadena);
			}
		}
		if (e.getSource().equals(vista.getBtnCE())) {
			vista.getTextFieldIngreso().setText("");
		}
		if (e.getSource().equals(vista.getBtn1sobreX())) {
			String cadena = vista.getTextFieldIngreso().getText();
			Double nro;
			if (cadena.length() > 0) {
				nro = 1 / (Double.parseDouble(cadena));
				vista.getTextFieldIngreso().setText(nro.toString());
			}
		}
		if (e.getSource().equals(vista.getBtnAproximacion())) {
			String cadena = vista.getTextFieldIngreso().getText();
			Double nro;
			if (cadena.length() > 0) {
				nro = (-1) * Double.parseDouble(cadena);
				vista.getTextFieldIngreso().setText(nro.toString());
			}

		}
		if (e.getSource().equals(vista.getBtnSumar())) {
			if (!vista.getTextFieldIngreso().getText().equals("")) {
				this.getCalculadora().setNumero1(vista.getTextFieldIngreso().getText());
				this.getCalculadora().setSigno("+");
				vista.getTextFieldIngreso().setText("");

			}

		}
		if (e.getSource().equals(vista.getBtnRestar())) {
			if (!vista.getTextFieldIngreso().getText().equals("")) {
				this.getCalculadora().setNumero1(vista.getTextFieldIngreso().getText());
				this.getCalculadora().setSigno("-");
				vista.getTextFieldIngreso().setText("");
			}
		}
		if (e.getSource().equals(vista.getBtnMultiplicar())) {
			if (!vista.getTextFieldIngreso().getText().equals("")) {
				this.getCalculadora().setNumero1(vista.getTextFieldIngreso().getText());
				this.getCalculadora().setSigno("*");
				vista.getTextFieldIngreso().setText("");
			}
		}
		if (e.getSource().equals(vista.getBtnDividir())) {
			if (!vista.getTextFieldIngreso().getText().equals("")) {
				this.getCalculadora().setNumero1(vista.getTextFieldIngreso().getText());
				this.getCalculadora().setSigno("/");
				vista.getTextFieldIngreso().setText("");
			}
		}
		if (e.getSource().equals(vista.getBtnIgualdad())) {
			String resultado;
			Double resultadoPorcentaje;
			this.getCalculadora().setNumero2(vista.getTextFieldIngreso().getText());
			if (!this.getCalculadora().getNumero1().equals("")) {
				resultado = this.getCalculadora().calcularNros(this.getCalculadora().getNumero1(),
						this.getCalculadora().getNumero2(), this.getCalculadora().getSigno());
				vista.getTextFieldIngreso().setText(resultado);

			}

		}
		if (e.getSource().equals(vista.getBtnPotenciaPor2())) {
			String cadena = vista.getTextFieldIngreso().getText();
			Double nro;
			if (cadena.length() > 0) {
				nro = Double.parseDouble(cadena) * Double.parseDouble(cadena);
				vista.getTextFieldIngreso().setText(nro.toString());
			}
		}
		if (e.getSource().equals(vista.getBtnRaizCuadradaX())) {
			String cadena = vista.getTextFieldIngreso().getText();
			Double nro;
			if (cadena.length() > 0) {
				nro = Math.sqrt(Double.parseDouble(cadena));
				vista.getTextFieldIngreso().setText(nro.toString());
			}
		}
		if (e.getSource().equals(vista.getBtnC())) {
			vista.getTextFieldIngreso().setText("");
			this.getCalculadora().setNumero1("");
		}
		if (e.getSource().equals(vista.getBtnPorcentaje())) {
			Double resultadoPorcentaje;
			String resultado;

			this.getCalculadora().setNumero2(vista.getTextFieldIngreso().getText());
			if (e.getSource().equals(vista.getBtnPorcentaje())) {
				try {
					resultadoPorcentaje = Double.parseDouble(this.getCalculadora().getNumero2()) * 100
							/ Double.parseDouble(this.getCalculadora().getNumero1());
					resultado = resultadoPorcentaje.toString();
					vista.getTextFieldIngreso().setText(resultado);
				} catch (NullPointerException r) {
					System.out.println("Ingrese 2 valores y el segundo ponga porcentaje:");
				}

			}
		}
	}

	public boolean existePunto(String cadena) {
		boolean rta = false;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.substring(i, i + 1).equals(".")) {
				rta = true;
			}
		}
		return rta;
	}
}
