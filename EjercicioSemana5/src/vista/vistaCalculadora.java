package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorCalculadora;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class vistaCalculadora extends JFrame {

	private JPanel contentPane;
	private ControladorCalculadora controlador;
	private JTextField textFieldIngreso;
	private JButton btnPorcentaje;
	private JButton btnCE;
	private JButton btnC;
	private JButton btnBack;
	private JButton btn1sobreX;
	private JButton btnPotenciaPor2;
	private JButton btnRaizCuadradaX;
	private JButton btnDividir;
	private JButton btn7;
	private JButton btn8;
	private JButton btn9;
	private JButton btnMultiplicar;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btnRestar;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btnSumar;
	private JButton btnAproximacion;
	private JButton btn0;
	private JButton btnPunto;
	private JButton btnIgualdad;

	public vistaCalculadora(ControladorCalculadora controlador) {
		setTitle("Calculadora");
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 280);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		textFieldIngreso = new JTextField();
		textFieldIngreso.setHorizontalAlignment(SwingConstants.RIGHT);
		textFieldIngreso.setBounds(10, 11, 261, 30);
		contentPane.add(textFieldIngreso);
		textFieldIngreso.setColumns(10);

		btnPorcentaje = new JButton("%");
		btnPorcentaje.addActionListener(this.getControlador());
		btnPorcentaje.setBounds(10, 52, 60, 30);
		contentPane.add(btnPorcentaje);

		btnCE = new JButton("CE");
		btnCE.addActionListener(this.getControlador());
		btnCE.setBounds(74, 52, 60, 30);
		contentPane.add(btnCE);

		btnC = new JButton("C");
		btnC.addActionListener(this.getControlador());
		btnC.setBounds(144, 52, 60, 30);
		contentPane.add(btnC);

		btnBack = new JButton("<<");
		btnBack.addActionListener(this.getControlador());
		btnBack.setBounds(211, 52, 60, 30);
		contentPane.add(btnBack);

		btn1sobreX = new JButton("1/x");
		btn1sobreX.addActionListener(this.getControlador());
		btn1sobreX.setBounds(10, 83, 60, 30);
		contentPane.add(btn1sobreX);

		btnPotenciaPor2 = new JButton("x^2");
		btnPotenciaPor2.addActionListener(this.getControlador());
		btnPotenciaPor2.setBounds(74, 83, 60, 30);
		contentPane.add(btnPotenciaPor2);

		btnRaizCuadradaX = new JButton("2\u221Ax");
		btnRaizCuadradaX.addActionListener(this.getControlador());
		btnRaizCuadradaX.setBounds(144, 83, 60, 30);
		contentPane.add(btnRaizCuadradaX);

		btnDividir = new JButton("\u00F7");
		btnDividir.addActionListener(this.getControlador());
		btnDividir.setBounds(211, 83, 60, 30);
		contentPane.add(btnDividir);

		btn7 = new JButton("7");
		btn7.addActionListener(this.getControlador());
		btn7.setBounds(10, 114, 60, 30);
		contentPane.add(btn7);

		btn8 = new JButton("8");
		btn8.addActionListener(this.getControlador());
		btn8.setBounds(74, 114, 60, 30);
		contentPane.add(btn8);

		btn9 = new JButton("9");
		btn9.addActionListener(this.getControlador());
		btn9.setBounds(144, 114, 60, 30);
		contentPane.add(btn9);

		btnMultiplicar = new JButton("X");
		btnMultiplicar.addActionListener(this.getControlador());
		btnMultiplicar.setBounds(211, 114, 60, 30);
		contentPane.add(btnMultiplicar);

		btn4 = new JButton("4");
		btn4.addActionListener(this.getControlador());
		btn4.setBounds(10, 145, 60, 30);
		contentPane.add(btn4);

		btn5 = new JButton("5");
		btn5.addActionListener(this.getControlador());
		btn5.setBounds(74, 145, 60, 30);
		contentPane.add(btn5);

		btn6 = new JButton("6");
		btn6.addActionListener(this.getControlador());
		btn6.setBounds(144, 145, 60, 30);
		contentPane.add(btn6);

		btnRestar = new JButton("-");
		btnRestar.addActionListener(this.getControlador());
		btnRestar.setBounds(211, 145, 60, 30);
		contentPane.add(btnRestar);

		btn1 = new JButton("1");
		btn1.addActionListener(this.getControlador());
		btn1.setBounds(10, 176, 60, 30);
		contentPane.add(btn1);

		btn2 = new JButton("2");
		btn2.addActionListener(this.getControlador());
		btn2.setBounds(74, 176, 60, 30);
		contentPane.add(btn2);

		btn3 = new JButton("3");
		btn3.addActionListener(this.getControlador());
		btn3.setBounds(144, 176, 60, 30);
		contentPane.add(btn3);

		btnSumar = new JButton("+");
		btnSumar.addActionListener(this.getControlador());
		btnSumar.setBounds(211, 176, 60, 30);
		contentPane.add(btnSumar);

		btnAproximacion = new JButton("\u00B1");
		btnAproximacion.addActionListener(this.getControlador());
		btnAproximacion.setBounds(10, 207, 60, 30);
		contentPane.add(btnAproximacion);

		btn0 = new JButton("0");
		btn0.addActionListener(this.getControlador());
		btn0.setBounds(74, 207, 60, 30);
		contentPane.add(btn0);

		btnPunto = new JButton(".");
		btnPunto.addActionListener(this.getControlador());
		btnPunto.setBounds(144, 207, 60, 30);
		contentPane.add(btnPunto);

		btnIgualdad = new JButton("=");
		btnIgualdad.addActionListener(this.getControlador());
		btnIgualdad.setBounds(211, 207, 60, 30);
		contentPane.add(btnIgualdad);
	}

	public ControladorCalculadora getControlador() {
		return controlador;
	}

	public void setControlador(ControladorCalculadora controlador) {
		this.controlador = controlador;
	}

	public JTextField getTextFieldIngreso() {
		return textFieldIngreso;
	}

	public JButton getBtnPorcentaje() {
		return btnPorcentaje;
	}

	public JButton getBtnCE() {
		return btnCE;
	}

	public JButton getBtnC() {
		return btnC;
	}

	public JButton getBtnBack() {
		return btnBack;
	}

	public JButton getBtn1sobreX() {
		return btn1sobreX;
	}

	public JButton getBtnPotenciaPor2() {
		return btnPotenciaPor2;
	}

	public JButton getBtnRaizCuadradaX() {
		return btnRaizCuadradaX;
	}

	public JButton getBtnDividir() {
		return btnDividir;
	}

	public JButton getBtn7() {
		return btn7;
	}

	public JButton getBtn8() {
		return btn8;
	}

	public JButton getBtn9() {
		return btn9;
	}

	public JButton getBtnMultiplicar() {
		return btnMultiplicar;
	}

	public JButton getBtn4() {
		return btn4;
	}

	public JButton getBtn5() {
		return btn5;
	}

	public JButton getBtn6() {
		return btn6;
	}

	public JButton getBtnRestar() {
		return btnRestar;
	}

	public JButton getBtn1() {
		return btn1;
	}

	public JButton getBtn2() {
		return btn2;
	}

	public JButton getBtn3() {
		return btn3;
	}

	public JButton getBtnSumar() {
		return btnSumar;
	}

	public JButton getBtnAproximacion() {
		return btnAproximacion;
	}

	public JButton getBtn0() {
		return btn0;
	}

	public JButton getBtnPunto() {
		return btnPunto;
	}

	public JButton getBtnIgualdad() {
		return btnIgualdad;
	}

}
