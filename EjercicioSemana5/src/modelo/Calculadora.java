package modelo;

import javax.swing.JOptionPane;

public class Calculadora {
	String numero1;
	String numero2;
	String signo;

	public Calculadora() {
		super();
		this.setNumero1(numero1);
		this.setNumero2(numero2);
		this.setSigno(signo);

	}

	public String getNumero1() {
		return numero1;
	}

	public void setNumero1(String numero1) {
		this.numero1 = numero1;
	}

	public String getNumero2() {
		return numero2;
	}

	public void setNumero2(String numero2) {
		this.numero2 = numero2;
	}

	public String getSigno() {
		return signo;
	}

	public void setSigno(String signo) {
		this.signo = signo;
	}

	public String calcularNros(String numero1, String numero2, String signo) {
		Double resultado = 0.0;
		String rta;

		try {
			if (signo.equals("+")) {
				resultado = Double.parseDouble(numero1) + Double.parseDouble(numero2);
			}
			if (signo.equals("-")) {
				resultado = Double.parseDouble(numero1) - Double.parseDouble(numero2);
			}
			if (signo.equals("*")) {
				resultado = Double.parseDouble(numero1) * Double.parseDouble(numero2);
			}
			if (signo.equals("/")) {
				resultado = Double.parseDouble(numero1) / Double.parseDouble(numero2);
			}
		} catch (NumberFormatException e) {
			System.out.println("Excepcion: " + e);
			JOptionPane.showMessageDialog(null, "Ingrese numero despues del signo");

		}
		rta = resultado.toString();
		return rta;
	}


}
