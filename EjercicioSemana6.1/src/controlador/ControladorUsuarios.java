package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.RowFilter;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import modelo.GestorUsuarios;
import modelo.Usuario;
import vista.vistaUsuarios;

public class ControladorUsuarios implements ListSelectionListener, ActionListener, KeyListener {

	private vistaUsuarios vista;
	private GestorUsuarios usuarios;

	public ControladorUsuarios(GestorUsuarios gus) {
		super();
		this.setVista(new vistaUsuarios(this));
		usuarios = gus;

		set();
		this.getVista().setVisible(true);
	}

	public vistaUsuarios getVista() {
		return vista;
	}

	public GestorUsuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(GestorUsuarios usuarios) {
		this.usuarios = usuarios;
	}

	public void setVista(vistaUsuarios vista) {
		this.vista = vista;
	}

	public void set() {
		Usuario u = new Usuario(07, "Lalo", "Landa", 777777);
		Object[] row2 = { String.valueOf(u.getId()), u.getNombre(), u.getApellido(), String.valueOf(u.getTelefono()) };
		this.getVista().getModeloTabla().addRow(row2);

		for (Usuario usuario : this.getUsuarios().getUsuarios()) {
			Object[] row = { String.valueOf(usuario.getId()), usuario.getNombre(), usuario.getApellido(),
					String.valueOf(usuario.getTelefono()) };
			this.getVista().getModeloTabla().addRow(row);
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		this.getVista().getBtnModificar().setEnabled(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource().equals(this.getVista().getBtnNuevo())) {
			ControladorNuevoUsuario cnu = new ControladorNuevoUsuario(this.getUsuarios());

			vista.dispose();
		}
		if (e.getSource().equals(this.getVista().getBtnEliminar())) {
			this.getVista().getModeloTabla().removeRow(this.getVista().getTable().getSelectedRow());
			
	
			/*String id = (this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 0).toString());
			int id2 = Integer.parseInt(id);
			for (Usuario usuario : this.getUsuarios().getUsuarios()) {
				if(usuario.getId().equals(id2)) {
					this.getUsuarios().getUsuarios().remove(usuario);
				}
				
			}
			*/

		}
		if (e.getSource().equals(this.getVista().getBtnModificar())) {
			System.out.println(this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 0));
			String id = (this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 0)
					.toString());
			int id2 = Integer.parseInt(id);
			String nombre = (String) this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(),
					1);
			String apellido = (String) this.getVista().getTable()
					.getValueAt(this.getVista().getTable().getSelectedRow(), 2);
			String telefono = (this.getVista().getTable().getValueAt(this.getVista().getTable().getSelectedRow(), 3)
					.toString());
			int tel = Integer.parseInt(telefono);
			Usuario u = new Usuario(id2, nombre, apellido, tel);
			ControladorModificarUsuario cmu = new ControladorModificarUsuario(u, this.getUsuarios());

			this.getVista().getModeloTabla().removeRow(this.getVista().getTable().getSelectedRow());

			vista.dispose();
		}

	}

	public void search() {
		TableRowSorter<DefaultTableModel> tr = new TableRowSorter<DefaultTableModel>(this.getVista().getModeloTabla());
		this.getVista().getTable().setRowSorter(tr);
		tr.setRowFilter(RowFilter.regexFilter(this.getVista().getTextField().getText()));
	}

	@Override
	public void keyPressed(KeyEvent e) {
		this.search();

	}

	@Override
	public void keyReleased(KeyEvent e) {
		this.search();

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

}
