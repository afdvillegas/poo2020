package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.security.SecureRandom;

import modelo.GestorUsuarios;
import modelo.Usuario;
import vista.vistaModificarUsuario;
import vista.vistaNuevoUsuario;

public class ControladorModificarUsuario implements ActionListener {
	private vistaModificarUsuario vista;
	private GestorUsuarios usuarios;

	public ControladorModificarUsuario(Usuario u,GestorUsuarios gu) {
		super();
		usuarios = gu;
		this.setVista(new vistaModificarUsuario(this));
		this.getVista().getTextFieldNombre().setText(u.getNombre());
		this.getVista().getTextFieldApellido().setText(u.getApellido());
		this.getVista().getTextFieldTelefono().setText(u.getTelefono().toString());

		this.getVista().setVisible(true);
	}

	public vistaModificarUsuario getVista() {
		return vista;
	}

	public void setVista(vistaModificarUsuario vista) {
		this.vista = vista;
	}

	public GestorUsuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(GestorUsuarios usuarios) {
		this.usuarios = usuarios;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		SecureRandom sr = new SecureRandom();
		Integer id = sr.nextInt(100);
		String nombre;
		String apellido;
		Integer telefono;
		nombre = vista.getTextFieldNombre().getText();
		apellido = vista.getTextFieldApellido().getText();
		telefono = Integer.parseInt(vista.getTextFieldTelefono().getText());
		Usuario u = new Usuario(id, nombre, apellido, telefono);
		this.getUsuarios().agregarUsuario(u);
		ControladorUsuarios c = new ControladorUsuarios(this.getUsuarios());
		//c.getVista().getModeloTabla().setValueAt(nombre, c.getVista().getTable().getSelectedRow(), 2);
		
		vista.dispose();
	}

}
