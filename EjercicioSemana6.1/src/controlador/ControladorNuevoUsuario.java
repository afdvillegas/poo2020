package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.security.SecureRandom;

import javax.swing.JOptionPane;
import javax.swing.border.LineBorder;

import modelo.GestorUsuarios;
import modelo.Usuario;
import vista.vistaNuevoUsuario;

public class ControladorNuevoUsuario implements ActionListener, FocusListener {
	private vistaNuevoUsuario vista;
	private GestorUsuarios usuarios;

	public ControladorNuevoUsuario(GestorUsuarios gus) {
		super();
		usuarios = gus;
		this.setVista(new vistaNuevoUsuario(this));

		this.getVista().setVisible(true);
	}

	public vistaNuevoUsuario getVista() {
		return vista;
	}

	public void setVista(vistaNuevoUsuario vista) {
		this.vista = vista;
	}

	public GestorUsuarios getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(GestorUsuarios usuarios) {
		this.usuarios = usuarios;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(this.getVista().getBtnCancelar())) {
			new ControladorUsuarios(this.getUsuarios());
			vista.dispose();
		}
		if (e.getSource().equals(this.getVista().getBtnAceptar())) {
			SecureRandom sr = new SecureRandom();
			Integer id = sr.nextInt(100);
			String nombre;
			String apellido;
			Integer telefono;
			nombre = vista.getTextFieldNombre().getText();
			apellido = vista.getTextFieldApellido().getText();
			try {
				telefono = Integer.parseInt(vista.getTextFieldTelefono().getText());
				Usuario u = new Usuario(id, nombre, apellido, telefono);
				this.getUsuarios().agregarUsuario(u);
			}catch (NumberFormatException error) {
				JOptionPane.showMessageDialog(null, "No ingresa valores enteros para el telefono.");
			}
			
			
			
			System.out.println("Controlador nuevo usuario");
			System.out.println(this.getUsuarios().getUsuarios().size());

			new ControladorUsuarios(this.getUsuarios());
			vista.dispose();

		}
	}

	@Override
	public void focusGained(FocusEvent e) {
		if (e.getSource().equals(this.getVista().getTextFieldNombre())) {
			this.getVista().getTextFieldNombre().setBorder(new LineBorder(Color.black));
		}
		if (e.getSource().equals(this.getVista().getTextFieldApellido())) {
			this.getVista().getTextFieldApellido().setBorder(new LineBorder(Color.black));
		}
		if (e.getSource().equals(this.getVista().getTextFieldTelefono())) {
			this.getVista().getTextFieldTelefono().setBorder(new LineBorder(Color.black));
		}

	}

	@Override
	public void focusLost(FocusEvent e) {
		if (e.getSource().equals(this.getVista().getTextFieldNombre())) {
			if (this.getVista().getTextFieldNombre().getText().equals("")) {
				this.getVista().getTextFieldNombre().setBorder(new LineBorder(Color.red));
				this.getVista().getLblErrorNombre().setVisible(true);
			} else {
				this.getVista().getTextFieldNombre().setBorder(new LineBorder(Color.GREEN));
				this.getVista().getLblErrorNombre().setVisible(false);
			}

		} else if (e.getSource().equals(this.getVista().getTextFieldApellido())) {
			if (this.getVista().getTextFieldApellido().getText().equals("")) {
				this.getVista().getTextFieldApellido().setBorder(new LineBorder(Color.red));
				this.getVista().getLblErrorApellido().setVisible(true);
			} else {
				this.getVista().getTextFieldApellido().setBorder(new LineBorder(Color.GREEN));
				this.getVista().getLblErrorApellido().setVisible(false);
			}
		} else if (e.getSource().equals(this.getVista().getTextFieldTelefono())) {
			if (this.getVista().getTextFieldTelefono().getText().equals("")) {
				this.getVista().getTextFieldTelefono().setBorder(new LineBorder(Color.red));
				this.getVista().getLblErrorTelefono().setVisible(true);
			} else {
				this.getVista().getTextFieldTelefono().setBorder(new LineBorder(Color.GREEN));
				this.getVista().getLblErrorTelefono().setVisible(false);
			}
		}
	}

}
