package controlador;

import java.awt.EventQueue;

import modelo.GestorUsuarios;
import vista.vistaUsuarios;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GestorUsuarios g = new GestorUsuarios();
					new ControladorUsuarios(g);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
