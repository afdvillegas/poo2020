package modelo;

public interface Observable {

	public void notifyObservers();
}
