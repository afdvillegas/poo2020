package modelo;

import java.util.ArrayList;
import java.util.Observable;

import controlador.ControladorNuevoUsuario;
import controlador.ControladorUsuarios;

public class GestorUsuarios extends Observable {

	ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

	public ArrayList<Usuario> getUsuarios() {
		return usuarios;
	}

	public void setUsuarios(ArrayList<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public void agregarUsuario(Usuario u) {
		getUsuarios().add(u);
		notifyObservers();
	}

}
