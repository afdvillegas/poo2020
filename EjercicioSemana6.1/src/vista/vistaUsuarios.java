package vista;

import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import controlador.ControladorUsuarios;
import modelo.GestorUsuarios;

public class vistaUsuarios extends JFrame implements Observer {

	private JPanel contentPane;
	private JTextField textFieldUsuarios;
	private JTable table;
	private JButton btnNuevo;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JButton btnInformar;
	private DefaultTableModel modeloTabla;
	private ControladorUsuarios cu;

	public vistaUsuarios(ControladorUsuarios cu) {
		this.setCu(cu);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblUsuarios = new JLabel("Usuarios");
		lblUsuarios.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblUsuarios.setBounds(146, 0, 77, 14);
		contentPane.add(lblUsuarios);

		btnNuevo = new JButton("Nuevo");
		btnNuevo.addActionListener(this.getCu());
		btnNuevo.setBounds(335, 43, 89, 23);
		contentPane.add(btnNuevo);

		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(this.getCu());
		btnModificar.setEnabled(false);
		btnModificar.setBounds(335, 77, 89, 23);
		contentPane.add(btnModificar);

		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(this.getCu());
		btnEliminar.setBounds(335, 111, 89, 23);
		contentPane.add(btnEliminar);

		btnInformar = new JButton("Informar");
		btnInformar.addActionListener(this.getCu());
		btnInformar.setBounds(335, 145, 89, 23);
		contentPane.add(btnInformar);

		JLabel lblBuscar = new JLabel("Buscar");
		lblBuscar.setBounds(83, 25, 48, 14);
		contentPane.add(lblBuscar);

		textFieldUsuarios = new JTextField();
		textFieldUsuarios.setBounds(137, 22, 121, 20);
		contentPane.add(textFieldUsuarios);
		textFieldUsuarios.setColumns(10);
		textFieldUsuarios.addKeyListener(this.getCu());

		JPanel panel = new JPanel();
		panel.setBounds(10, 50, 306, 201);
		contentPane.add(panel);
		panel.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 306, 193);
		panel.add(scrollPane);
		String header[] = { "ID", "Nombre", "Apellido", "Telefono" };
		this.setModeloTabla(new DefaultTableModel(null, header));

		table = new JTable(this.getModeloTabla());
		table.setBounds(0, 0, 306, 193);

		// Eliminar columna ID
		table.getColumnModel().getColumn(0).setMaxWidth(0);
		table.getColumnModel().getColumn(0).setMinWidth(0);
		table.getColumnModel().getColumn(0).setPreferredWidth(0);

		// Para que no deje editar las columnas
		table.setDefaultEditor(Object.class, null);

		// Para que no se seleccione mas de una sola fila
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		// Controlador de ListerSelectionListener
		table.getSelectionModel().addListSelectionListener(this.getCu());

		// Para que tome el scrollPane dentro de la tabla
		scrollPane.setViewportView(table);

	}

	public JTextField getTextField() {
		return textFieldUsuarios;
	}

	public void setTextField(JTextField textField) {
		this.textFieldUsuarios = textField;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JButton getBtnNuevo() {
		return btnNuevo;
	}

	public void setBtnNuevo(JButton btnNuevo) {
		this.btnNuevo = btnNuevo;
	}

	public JButton getBtnModificar() {
		return btnModificar;
	}

	public void setBtnModificar(JButton btnModificar) {
		this.btnModificar = btnModificar;
	}

	public JButton getBtnEliminar() {
		return btnEliminar;
	}

	public void setBtnEliminar(JButton btnEliminar) {
		this.btnEliminar = btnEliminar;
	}

	public JButton getBtnInformar() {
		return btnInformar;
	}

	public void setBtnInformar(JButton btnInformar) {
		this.btnInformar = btnInformar;
	}

	public DefaultTableModel getModeloTabla() {
		return modeloTabla;
	}

	public void setModeloTabla(DefaultTableModel modeloTabla) {
		this.modeloTabla = modeloTabla;
	}

	public ControladorUsuarios getCu() {
		return cu;
	}

	public void setCu(ControladorUsuarios cu) {
		this.cu = cu;
	}

	@Override
	public void update(Observable o, Object arg) {
		GestorUsuarios ge = (GestorUsuarios) o;
		
	}

}
