package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import controlador.ControladorNuevoUsuario;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class vistaNuevoUsuario extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JTextField textFieldTelefono;
	private ControladorNuevoUsuario cnu;
	private JButton btnAceptar;
	private JButton btnCancelar;
	private JLabel lblErrorNombre;
	private JLabel lblErrorApellido;
	private JLabel lblErrorTelefono;

	public vistaNuevoUsuario(ControladorNuevoUsuario cnu) {
		this.setCnu(cnu);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 320);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNuevoUsuario = new JLabel("Nuevo Usuario");
		lblNuevoUsuario.setBounds(10, 11, 132, 14);
		lblNuevoUsuario.setFont(new Font("Arial", Font.BOLD, 14));
		contentPane.add(lblNuevoUsuario);

		JLabel lblnombre = new JLabel("*Nombre:");
		lblnombre.setBounds(10, 36, 73, 14);
		contentPane.add(lblnombre);

		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(10, 61, 186, 20);
		textFieldNombre.addFocusListener(this.getCnu());
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		JLabel lblapellido = new JLabel("*Apellido:");
		lblapellido.setBounds(10, 92, 73, 14);
		contentPane.add(lblapellido);

		textFieldApellido = new JTextField();
		textFieldApellido.setBounds(10, 117, 186, 20);
		textFieldApellido.addFocusListener(this.getCnu());
		contentPane.add(textFieldApellido);
		textFieldApellido.setColumns(10);

		JLabel lbltelefono = new JLabel("*Telefono:");
		lbltelefono.setBounds(10, 148, 73, 14);
		contentPane.add(lbltelefono);

		textFieldTelefono = new JTextField();
		textFieldTelefono.setBounds(10, 173, 186, 20);
		textFieldTelefono.addFocusListener(this.getCnu());
		contentPane.add(textFieldTelefono);
		textFieldTelefono.setColumns(10);

		JLabel lblCampos = new JLabel("* Campos obligatorios");
		lblCampos.setBounds(10, 219, 132, 14);
		contentPane.add(lblCampos);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this.getCnu());
		btnCancelar.setBounds(187, 250, 87, 23);
		contentPane.add(btnCancelar);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this.getCnu());
		btnAceptar.setBounds(101, 250, 82, 23);
		contentPane.add(btnAceptar);
		
		lblErrorNombre = new JLabel("Ingrese nombre");
		lblErrorNombre.setForeground(Color.RED);
		lblErrorNombre.setBounds(65, 33, 118, 20);
		contentPane.add(lblErrorNombre);
		
		lblErrorApellido = new JLabel("Ingrese apellido");
		lblErrorApellido.setForeground(Color.RED);
		lblErrorApellido.setBounds(65, 92, 118, 14);
		contentPane.add(lblErrorApellido);
		
		lblErrorTelefono = new JLabel("Ingrese telefono");
		lblErrorTelefono.setForeground(Color.RED);
		lblErrorTelefono.setBounds(65, 148, 118, 14);
		contentPane.add(lblErrorTelefono);
		
		this.getLblErrorNombre().setVisible(false);
		this.getLblErrorApellido().setVisible(false);
		this.getLblErrorTelefono().setVisible(false);
	}

	public ControladorNuevoUsuario getCnu() {
		return cnu;
	}

	public void setCnu(ControladorNuevoUsuario cnu) {
		this.cnu = cnu;
	}

	public JTextField getTextFieldNombre() {
		return textFieldNombre;
	}

	public void setTextFieldNombre(JTextField textFieldNombre) {
		this.textFieldNombre = textFieldNombre;
	}

	public JTextField getTextFieldApellido() {
		return textFieldApellido;
	}

	public void setTextFieldApellido(JTextField textFieldApellido) {
		this.textFieldApellido = textFieldApellido;
	}

	public JTextField getTextFieldTelefono() {
		return textFieldTelefono;
	}

	public void setTextFieldTelefono(JTextField textFieldTelefono) {
		this.textFieldTelefono = textFieldTelefono;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	public JLabel getLblErrorNombre() {
		return lblErrorNombre;
	}

	public void setLblErrorNombre(JLabel lblErrorNombre) {
		this.lblErrorNombre = lblErrorNombre;
	}

	public JLabel getLblErrorApellido() {
		return lblErrorApellido;
	}

	public void setLblErrorApellido(JLabel lblErrorApellido) {
		this.lblErrorApellido = lblErrorApellido;
	}

	public JLabel getLblErrorTelefono() {
		return lblErrorTelefono;
	}

	public void setLblErrorTelefono(JLabel lblErrorTelefono) {
		this.lblErrorTelefono = lblErrorTelefono;
	}
	
	
}
