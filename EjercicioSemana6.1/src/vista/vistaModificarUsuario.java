package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controlador.ControladorModificarUsuario;
import controlador.ControladorNuevoUsuario;
import modelo.GestorUsuarios;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;
import java.awt.event.ActionEvent;

public class vistaModificarUsuario extends JFrame implements Observer{

	private JPanel contentPane;
	private JTextField textFieldNombre;
	private JTextField textFieldApellido;
	private JTextField textFieldTelefono;
	private ControladorModificarUsuario cmu;
	private JButton btnAceptar;
	private JButton btnCancelar;

	public vistaModificarUsuario(ControladorModificarUsuario cmu) {
		this.setCmu(cmu);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 320);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblModificarUsuario = new JLabel("Modificar Usuario");
		lblModificarUsuario.setBounds(10, 11, 132, 14);
		lblModificarUsuario.setFont(new Font("Arial", Font.BOLD, 14));
		contentPane.add(lblModificarUsuario);

		JLabel lblnombre = new JLabel("*Nombre:");
		lblnombre.setBounds(10, 36, 73, 14);
		contentPane.add(lblnombre);

		textFieldNombre = new JTextField();
		textFieldNombre.setBounds(10, 61, 186, 20);
		contentPane.add(textFieldNombre);
		textFieldNombre.setColumns(10);

		JLabel lblapellido = new JLabel("*Apellido:");
		lblapellido.setBounds(10, 92, 73, 14);
		contentPane.add(lblapellido);

		textFieldApellido = new JTextField();
		textFieldApellido.setBounds(10, 117, 186, 20);
		contentPane.add(textFieldApellido);
		textFieldApellido.setColumns(10);

		JLabel lbltelefono = new JLabel("*Telefono:");
		lbltelefono.setBounds(10, 148, 73, 14);
		contentPane.add(lbltelefono);

		textFieldTelefono = new JTextField();
		textFieldTelefono.setBounds(10, 173, 186, 20);
		contentPane.add(textFieldTelefono);
		textFieldTelefono.setColumns(10);

		JLabel lblCampos = new JLabel("* Campos obligatorios");
		lblCampos.setBounds(10, 219, 132, 14);
		contentPane.add(lblCampos);

		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(this.getCmu());
		btnCancelar.setBounds(187, 250, 87, 23);
		contentPane.add(btnCancelar);

		btnAceptar = new JButton("Aceptar");
		btnAceptar.addActionListener(this.getCmu());
		btnAceptar.setBounds(101, 250, 82, 23);
		contentPane.add(btnAceptar);
	}

	public ControladorModificarUsuario getCmu() {
		return cmu;
	}

	public void setCmu(ControladorModificarUsuario cmu) {
		this.cmu = cmu;
	}

	public JTextField getTextFieldNombre() {
		return textFieldNombre;
	}

	public void setTextFieldNombre(JTextField textFieldNombre) {
		this.textFieldNombre = textFieldNombre;
	}

	public JTextField getTextFieldApellido() {
		return textFieldApellido;
	}

	public void setTextFieldApellido(JTextField textFieldApellido) {
		this.textFieldApellido = textFieldApellido;
	}

	public JTextField getTextFieldTelefono() {
		return textFieldTelefono;
	}

	public void setTextFieldTelefono(JTextField textFieldTelefono) {
		this.textFieldTelefono = textFieldTelefono;
	}

	public JButton getBtnAceptar() {
		return btnAceptar;
	}

	public void setBtnAceptar(JButton btnAceptar) {
		this.btnAceptar = btnAceptar;
	}

	public JButton getBtnCancelar() {
		return btnCancelar;
	}

	public void setBtnCancelar(JButton btnCancelar) {
		this.btnCancelar = btnCancelar;
	}

	@Override
	public void update(Observable o, Object arg) {
		GestorUsuarios ge = (GestorUsuarios) o;
		
	}

}
