import java.util.InputMismatchException;
import java.util.Scanner;

public abstract class Vehiculo {
	private String patente;
	private String modelo;
	private String averia;
	private String tipoVehiculo;
	private Integer horasReparacion;

	public Vehiculo(String patente, String modelo, String averia, String tipoVehiculo, Integer horasReparacion) {
		super();
		this.setPatente(patente);
		this.setModelo(modelo);
		this.setAveria(averia);
		this.setTipoVehiculo(tipoVehiculo);
		this.setHorasReparacion(horasReparacion);
	}

	protected String getPatente() {
		return patente;
	}

	protected void setPatente(String patente) {
		this.patente = patente;
	}

	protected String getModelo() {
		return modelo;
	}

	protected void setModelo(String modelo) {
		this.modelo = modelo;
	}

	protected String getAveria() {
		return averia;
	}

	protected void setAveria(String averia) {
		this.averia = averia;
	}

	protected Integer getHorasReparacion() {
		return horasReparacion;
	}

	protected void setHorasReparacion(Integer horasReparacion) {
		// Tratar las exepciones en otro lado(propagarlas)
		Scanner s = new Scanner(System.in);
		Boolean ciclo = true;
		do {
			try {

				if (horasReparacion <= 0) {
					throw new RangoHoras(100);
				} else {
					ciclo = false;
				}

				this.horasReparacion = horasReparacion;

			} catch (RangoHoras e) {
				System.out.println(e.getMessage());
				System.out.println("Ingrese nueva hora: ");
				horasReparacion = s.nextInt();

			}
		} while (ciclo);

	}

	protected String getTipoVehiculo() {
		return tipoVehiculo;
	}

	protected void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public abstract Double recargo();

	public String mostrarInfo() {
		return "Vehiculo: " + getPatente() + " Modelo: " + getModelo() + " Averia: " + getAveria()
				+ " HorasReparacion: " + getHorasReparacion() + " TipoVehiculo: " + getTipoVehiculo();
	}

}
