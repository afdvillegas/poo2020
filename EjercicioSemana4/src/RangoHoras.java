
public class RangoHoras extends Exception {
	private Integer code;

	public RangoHoras(Integer code) {
		super();
		this.setCode(code);

	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	public String getMessage() {
		String mensaje= "";
		if(this.getCode()==100) {
			mensaje = "Las horas ingresadas son inferior a 0";
		}
		return mensaje;
	}

}
