
public class Due�o {
	private Integer dni;
	private String nombre;
	private String contacto;
	private Vehiculo vehiculo;

	public Due�o(Integer dni, Vehiculo vehiculo) {
		super();
		this.setDni(dni);
		this.setNombre(nombre);
		this.setContacto(contacto);
		this.setVehiculo(vehiculo);
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	@Override
	public String toString() {
		return "Due�o: " + getDni() + "Nombre: " + getNombre() + "Contacto: " + getContacto()
				+ "Vehiculo con la patente: " + getVehiculo().getPatente();
	}

}
