import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Formatter;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Taller {

	private ArrayList<Vehiculo> vehiculos;
	private ArrayList<Due�o> due�os;

	public Taller() {
		super();
		this.setVehiculos(new ArrayList<>());
		this.setDue�os(new ArrayList<>());
	}

	public ArrayList<Due�o> getDue�os() {
		return due�os;
	}

	public void setDue�os(ArrayList<Due�o> due�os) {
		this.due�os = due�os;
	}

	public ArrayList<Vehiculo> getVehiculos() {
		return vehiculos;
	}

	public void setVehiculos(ArrayList<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public String agregarVehiculo() {
		Scanner s = new Scanner(System.in);
		String patente;
		String modelo;
		String averia;
		String tipoVeh;
		Integer horasRep = 0;
		Boolean ciclo = true;

		System.out.println("Ingrese patente de vehiculo: ");
		patente = s.nextLine();
		System.out.println("Ingrese modelo: ");
		modelo = s.nextLine();

		do {
			System.out.println("Ingrese averia: ");
			averia = s.nextLine();
			switch (averia) {
			case "bomba de agua":
				ciclo = false;
				break;
			case "cable de embrague":
				ciclo = false;
				break;
			case "disco de frenos":
				ciclo = false;
				break;
			default:
				System.out.println("Ingrese uno de estos problemas: ");
				System.out.println("bomba de agua");
				System.out.println("cable de embrague");
				System.out.println("disco de frenos");
				break;
			}
		} while (ciclo);

		ciclo = true;
		do {
			System.out.println("Ingrese tipo de vehiculo: ");
			System.out.println("Auto");
			System.out.println("Camion");
			System.out.println("Camioneta");
			tipoVeh = s.nextLine();
			if (tipoVeh.equals("Auto") || tipoVeh.equals("Camion") || tipoVeh.equals("Camioneta")) {
				ciclo = false;
			}
		} while (ciclo);
		ciclo = true;

		do {
			try {
				System.out.println("Ingrese horas estimadas para su reparacion: ");
				horasRep = s.nextInt();
				if (horasRep <= 0) {
					throw new RangoHoras(100);
				} else {
					ciclo = false;
				}
			} catch (InputMismatchException e) {
				System.out.println("Excepcion: " + e);
				s.nextLine();
				System.out.println("Ingrese numero entero");
			} catch (RangoHoras e) {
				System.out.println(e.getMessage());
				System.out.println("Ingrese nueva hora: ");
			}

		} while (ciclo);

		switch (tipoVeh) {
		case "Auto":
			Auto a = new Auto(patente, modelo, averia, tipoVeh, horasRep);
			this.getVehiculos().add(a);
			asignarDue�o(a);
			System.out.println(a.mostrarInfo());

			break;
		case "Camion":
			Camion c = new Camion(patente, modelo, averia, tipoVeh, horasRep);
			this.getVehiculos().add(c);
			asignarDue�o(c);
			System.out.println(c.mostrarInfo());
			break;
		case "Camioneta":
			Camioneta ca = new Camioneta(patente, modelo, averia, tipoVeh, horasRep);
			this.getVehiculos().add(ca);
			asignarDue�o(ca);
			System.out.println(ca.mostrarInfo());

			break;
		default:
			System.out.println("El tipo de vehiculo especificado no se encuentra");
			break;
		}

		// this.getVehiculos().add(vehiculos);
		return "vehiculo agregado correctamente";
	}

	public void asignarDue�o(Vehiculo v) {
		Scanner s = new Scanner(System.in);
		Integer dni = 0;
		String nombre;
		String contacto;
		Boolean ciclo = true;

		do {
			try {
				System.out.println("Ingrese dni del due�o: ");
				dni = s.nextInt();
				ciclo = false;
			} catch (InputMismatchException e) {
				System.out.println("Excepcion: " + e);
				s.nextLine();
				System.out.println("Ingrese numero entero");
			}
		} while (ciclo);

		// Ingresar este salto de linea para que me tome el proximo nextLine (Error con
		// nextInt)
		s.nextLine();
		System.out.println("Ingrese nombre y apellido: ");
		nombre = s.nextLine();
		System.out.println("Ingrese contacto: ");
		contacto = s.nextLine();

		Due�o d = new Due�o(dni, v);
		d.setNombre(nombre);
		d.setContacto(contacto);
		this.getDue�os().add(d);
	}

	public void verDue�os() {
		for (Due�o due�o : due�os) {

			System.out.println("El due�o con el nombre: " + due�o.getNombre() + " tiene un vehiculo con la patente "
					+ due�o.getVehiculo().getPatente());
		}
	}

	public Double ingresarCosto(String patente) {

		for (Vehiculo vehiculo : vehiculos) {

			if (vehiculo.getPatente().equals(patente)) {

				if (vehiculo.getAveria().equals("bomba de agua")) {

					return 1000.00;

				} else if (vehiculo.getAveria().equals("cable de embrague")) {

					return 1500.00;

				} else {

					return 2000.00;
				}
			}

		}

		System.out.println("probando");
		return 0.0;

	}

	public void calcularPrecios() {
		Double valor;
		Double valorHoras;
		Double total;
		Double recargo;
		for (Vehiculo vehiculo : vehiculos) {

			
			valor = ingresarCosto(vehiculo.getPatente());
			valorHoras = (double) (vehiculo.getHorasReparacion() * 70.0);
			total = (valor + valorHoras);
			recargo = total * vehiculo.recargo();
			total = total + recargo;

			System.out.println("El vehiculo " + vehiculo.getPatente() + " con la aver�a " + vehiculo.getAveria()+"costar� "+total+" para su arreglo");
			
		}
	}

	public void generarArchivo() {
		String texto = "";
		try (Formatter output = new Formatter("assets/vehiculos.csv")) {

			for (Vehiculo vehiculo : vehiculos) {

				texto = vehiculo.getPatente() + ";" + vehiculo.getModelo() + ";" + vehiculo.getAveria() + ";"
						+ vehiculo.getTipoVehiculo() + ";" + +vehiculo.getHorasReparacion() + "\n";
				try {
					output.format("%s", texto);

				} catch (NoSuchElementException e) {
					System.out.println("El valor ingresado no es valido");

				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
