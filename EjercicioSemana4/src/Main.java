import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Taller t1 = new Taller();
		Boolean rta = true;
		Scanner s = new Scanner(System.in);

		while (rta == true) {
			t1.agregarVehiculo();
			System.out.println("�Desea agregar otro vehiculo? Pulse N para terminar");
			Character letra;
			letra = s.nextLine().charAt(0);

			if (letra == 'N') {
				rta = false;
			}

		}
		System.out.println("Se calcula los precios de cada vehiculo ingresado: ");
		t1.calcularPrecios();

		System.out.println("Se muestran los due�os con su auto");
		t1.verDue�os();

		System.out.println("Genera el archivo csv");
		t1.generarArchivo();
		// System.out.println(auto1.mostrarInfo());

	}

}
