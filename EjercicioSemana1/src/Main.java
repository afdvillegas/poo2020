import java.util.Scanner;

public class Main {

	/*
	 * // Operadores de asignacion compuestos
	 * 
	 * i += 7 (i = i + 7)
	 * 
	 * i -= 7 (i = i -7)
	 * 
	 * i *= 5 (i = i * 5)
	 * 
	 * i /= 5 (i = i/5)
	 * 
	 * i %= 9 (resto e igual)
	 * 
	 * ++i (incrementa en uno y usa el valor)
	 * 
	 * i++ (usa el valor y luego lo incrementa)
	 */

	public static void main(String[] args) {

		// Tipos primitivos
		Integer nro = 7;
		double nroFlotante = 5.5;
		boolean rta = false;
		char caracter = 'B';
		String frase = "Hola mundo";

		// Ejemplo Operadores logicos

		/*
		 * Para hacer la condicion (and) se usa && (todas las condiciones deben ser
		 * verdad para que se cumpla) o & para la primer condicion
		 * 
		 * Para hacer la condicion (or) se usa || (al menos una condicion debe ser
		 * verdad para que se cumpla) o | para la primer condicion
		 * 
		 */
		System.out.println("Ejemplo condiciones if");
		if (nro > nroFlotante && nro > 0) {
			System.out.println(nro + " es mayor a " + nroFlotante);

		} else if (nro == nroFlotante) {
			System.out.println("Los numeros son iguales");
		} else {
			System.out.println(nro + " es menor a " + nroFlotante);
		}

		// Ejemplo Switch multiples opciones
		System.out.println("\n");
		System.out.println("Ejemplo Switch");
		switch (caracter) {
		case 'A':
			System.out.println("El caracter es A");
			break;
		case 'B':
			System.out.println("El caracter es B");
			break;
		default:
			System.out.println("No es ninguno");

		}

		// Arreglos estaticos
		int[] numerosEnteros = { 1, 8, 4, 0, 7, 5, 7 };

		// Bucle while (Mostrar contenido arreglo)
		System.out.println("\n");
		System.out.println("Ejemplo Bucle While");
		int i = 0;
		System.out.println("Muestra contenido de arreglo: ");
		while (i < numerosEnteros.length) {
			System.out.println(numerosEnteros[i]);
			i++;
		}

		// Bucle for
		System.out.println("\n");
		System.out.println("Ejemplo for");
		System.out.println("Imprime la frase cada que recorre el arreglo");
		for (int k = 0; k < numerosEnteros.length; k++) {
			System.out.println(frase + " " + k);

		}

		// Bucle repetir
		System.out.println("\n");
		System.out.println("Ejemplo do (repetir)");
		System.out.println("Recorre el arreglo para encontrar el 0 e informar su posicion");

		int j = 6;
		do {
			if (numerosEnteros[j] == 0) {
				rta = true;
				System.out.println("Se encontro el 0 " + "en la posicion " + j + " del arreglo");

			}
			System.out.println("No es 0" + " en la posicion " + j);
			j = j - 1;
			// Condicion para que termine el bucle repetir
		} while (j >= 0 && !rta);

	}

	// Probando commit
	
}

/*Esta todo correcto*/
